﻿using CylinderHeating.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using CylinderHeatingLibrary;
using CylinderHeating.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CylinderHeating.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationContext _context;
        private int _userId;

        public HomeController(ILogger<HomeController> logger, ApplicationContext applicationContext)
        {
            _logger = logger;
            _context = applicationContext;
        }
        
        [HttpPost]
        public IActionResult Result(CylinderHeatingInput input)
        {
            bool check = CylinderHeatingSolve.CheckInputValues(input);
            if (!check)
            {
                TempData["message"] = $"Заполните все поля числами";
                return RedirectToAction(nameof(Index)); 
            }
            if (!string.IsNullOrEmpty(input.Name))
            {
                var existVariant = _context.Variants.FirstOrDefault(x => x.Name == input.Name);
                if (existVariant != null)
                {
                    existVariant.FurnaceTemp = input.FurnaceTemp;
                    existVariant.Diameter = input.Diameter;
                    existVariant.InitCylTemp = input.InitCylTemp;
                    existVariant.HeatConductCoeff = input.HeatConductCoeff;
                    existVariant.HeatCapacity = input.HeatCapacity;
                    existVariant.Density = input.Density;
                    existVariant.HeatTransferCoeff = input.HeatTransferCoeff;
                    existVariant.SurfaceTemp = input.SurfaceTemp;

                    _context.Variants.Update(existVariant);
                    _context.SaveChanges();
                }
                else if (LoginController._uid != 0)
                {
                    var variant = new Variant
                    {
                        Name = input.Name,
                        FurnaceTemp = input.FurnaceTemp,
                        Diameter = input.Diameter,
                        InitCylTemp = input.InitCylTemp,
                        HeatConductCoeff = input.HeatConductCoeff,
                        HeatCapacity = input.HeatCapacity,
                        Density = input.Density,
                        HeatTransferCoeff = input.HeatTransferCoeff,
                        SurfaceTemp = input.SurfaceTemp,
                        UserId = LoginController._uid,
                        CreatedAt = DateTime.Now
                    };
                    _context.Variants.Add(variant);
                    _context.SaveChanges();
                }
            }
            var BIO = _context.BioCoeffs.ToList();
            double[] ys = new double[23];
            var model = new CylinderHeatingBios()
            {
                Rows = new List<CylinderHeatingBiosRow>()
            };
            for (int i =0; i<15; i++) 
            {
                var row = new CylinderHeatingBiosRow
                {
                    Bio = BIO[i].Bio,
                    Mu  = BIO[i].Mu,
                    Pts = BIO[i].Pts,
                    Mts = BIO[i].Mts,
                    Nts = BIO[i].Nts
                };
                model.Rows.Add(row);
            }
            // Выполнение расчета
            var lib = new CylinderHeatingSolve(input, model);
            var result = lib.Solve();
            if (result.Rows.Count == 0) 
            {
                TempData["Message"] = "Расчет провести невозможно (ошибка на нахождение числа подобия Bi, либо температуры оси цилиндра), проверьте данные на корректность";
                return RedirectToAction("Index", "Home");
            }
            var r = result.Rows[result.Rows.Count-1];
            if (r.Op <= 0 || r.Op >= 1 || Double.IsNaN(r.Op))
            {
                TempData["Message"] = "Измените значения температуры! Темп. печи >= темп. поверхности >= нач. темп.";
                return RedirectToAction("Index", "Home");
            }
            if (r.Tc<=0)
            {
                TempData["Message"] = "Расчет провести невозможно (ошибка нахождения температуры оси цилиндра), проверьте данные на корректность";
                return RedirectToAction("Index", "Home");
            }
            return View(result);
        }


        [HttpGet]
        public IActionResult Index(int? variantId)
        {
            var viewModel = new HomeViewModel();
            var login = User.FindFirstValue(ClaimTypes.Name);
            var user = _context.Users.FirstOrDefault(x => x.Login == login);
            if (variantId != null)
            {
                viewModel.Variant = _context.Variants
                    .Where(x => x.UserId == user.Id )
                    .FirstOrDefault(x => x.Id == variantId);
            }
            if (user != null)
            {
                viewModel.Variants = _context.Variants
                    .Where(x => x.UserId == user.Id || x.UserId == 0).ToList();
            }
            else
            {
                viewModel.Variants = _context.Variants
                    .Where(x => x.UserId == 0)
                    .ToList();
            }

            return View(viewModel);
        }
        
        [HttpGet]
        public IActionResult Remove(int? variantId)
        {
            var login = User.FindFirstValue(ClaimTypes.Name);
            var user = _context.Users.FirstOrDefault(x => x.Login == login);
            var variant = _context.Variants
                .Where(x => x.UserId == user.Id)
                .FirstOrDefault(x => x.Id == variantId);

            if (variant != null)
            {
                _context.Variants.Remove(variant);
                _context.SaveChanges();

                TempData["message"] = $"Вариант пользователя {variant.Name} удален.";
            }
            else
            {
                TempData["message"] = $"Вариант не найден.";
            }

            return RedirectToAction(nameof(Index));
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}