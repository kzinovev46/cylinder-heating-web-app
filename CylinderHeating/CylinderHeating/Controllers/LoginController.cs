﻿using CylinderHeating.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using CylinderHeatingLibrary;
using CylinderHeating.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using RecuperatorLibrary;
using Microsoft.AspNetCore.Identity;

namespace CylinderHeating.Controllers
{

    public class LoginController : Controller
    {
        private readonly ILogger<LoginController> _logger;
        private readonly ApplicationContext _context;

        public static int _uid;
        public LoginController(ILogger<LoginController> logger, ApplicationContext applicationContext)
        {
            _logger = logger;
            _context = applicationContext;
        }
        [HttpPost]
        public async Task<IActionResult> Index(string login, string password)
        {
            string newPass = HashPass.hashPassword(password);
            var user = _context.Users.FirstOrDefault(x => x.Login == login && x.Password == newPass);
            if (user != null)
            {
                var claims = new List<Claim> {
                    new Claim("Id", user.Id.ToString()),
                    new Claim(ClaimTypes.Name, user.Login),
                    new Claim(ClaimTypes.Role, "user"),
                };
                ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Cookies");

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));
                _uid = user.Id;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["Message"] = "Неверно введены логин или пароль!";
            }
            // сделать вью с ошибкой
            return View(); 
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Register(string login, string name, string pass, string confirmPass)
        {

            var user = await _context.Users.FirstOrDefaultAsync(x => x.Login == login);
            if (user != null)
            {
                TempData["Message"] = "Данный email уже занят, попробуйте другой";
            }
            else if (pass != confirmPass)
                TempData["Message"] = "Пароли не совпадают!";
            else
            {
                string newPass = HashPass.hashPassword(pass);
                _context.Users.Add(new User { Name = name, Login = login, Password = newPass });
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Login");
            }

            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();

            return RedirectToAction("Index");
        }
    }
}



      