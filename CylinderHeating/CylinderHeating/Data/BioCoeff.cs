﻿using System.ComponentModel.DataAnnotations;

namespace CylinderHeating.Data
{
    public class BioCoeff
    {
        [Key]
        public double Bio { get; set; }
        public double Mu { get; set; }
        public double Pts { get; set; }
        public double Mts { get; set; }
        public double Nts { get; set; }
    }
}
