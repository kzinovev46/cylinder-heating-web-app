﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CylinderHeating.Data
{
    public class Variant
    {
        [Key]
        public int Id { get; set; }
        public int? UserId { get; set; } // если 0, то вариант публичный
        public string Name { get; set; }
        public double FurnaceTemp { get; set; } // Температура печи
        public double Diameter { get; set; } // Диаметр цилиндра
        public double InitCylTemp { get; set; } // Начальная температура цилиндра
        public double HeatConductCoeff { get; set; } // Коэффициент теплопроводности
        public double HeatCapacity { get; set; } // Теплоемкость
        public double Density { get; set; }  // Плотность
        public double HeatTransferCoeff { get; set; } // Коэффициент теплоотдачи
        public double SurfaceTemp { get; set; } // Температура поверхности цилиндра
        [Column(TypeName = "datetime")]
        public DateTime CreatedAt { get; set; } // Дата создания
    }
}
