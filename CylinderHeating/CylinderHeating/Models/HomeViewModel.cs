﻿using CylinderHeating.Data;

namespace CylinderHeating.Models
{
    public class HomeViewModel
    {
        public Variant? Variant { get; set; }

        public List<Variant> Variants { get; set; }
    }
}
