﻿
using System.Linq.Expressions;
using System.Net.NetworkInformation;


namespace CylinderHeatingLibrary
{
    public class CylinderHeatingSolve
    {
        public double FurnaceTemp { get; set; } // Температура печи
        public double Diameter { get; set; } // Диаметр цилиндра
        public double InitCylTemp { get; set; } // Начальная температура цилиндра
        public double HeatConductCoeff { get; set; } // Коэффициент теплопроводности
        public double HeatCapacity { get; set; } // Теплоемкость
        public double Density { get; set; }  // Плотность
        public double HeatTransferCoeff { get; set; } // Коэффициент теплоотдачи
        public double SurfaceTemp { get; set; } // Температура поверхности цилиндра
        public CylinderHeatingBios list { get; set; }

        public CylinderHeatingSolve(CylinderHeatingInput input, CylinderHeatingBios bio)
        {
            FurnaceTemp = input.FurnaceTemp;
            Diameter = input.Diameter;
            InitCylTemp = input.InitCylTemp;
            HeatConductCoeff = input.HeatConductCoeff;
            HeatCapacity = input.HeatCapacity;
            Density = input.Density;
            HeatTransferCoeff = input.HeatTransferCoeff;
            SurfaceTemp = input.SurfaceTemp;
            this.list = bio;
        }
        public static bool CheckInputValues(CylinderHeatingInput input)
        {
           
            if (input.FurnaceTemp != 0 && input.Diameter !=0 && input.InitCylTemp != 0 && input.HeatConductCoeff != 0 && input.HeatCapacity !=0 && input.Density !=0 && input.HeatTransferCoeff != 0 && input.SurfaceTemp != 0)
            {
                if (input.FurnaceTemp.GetType() == typeof(double) && input.Diameter.GetType() == typeof(double) && input.InitCylTemp.GetType() == typeof(double) && input.HeatConductCoeff.GetType() == typeof(double) && input.HeatCapacity.GetType() == typeof(double) && input.Density.GetType() == typeof(double) && input.HeatTransferCoeff.GetType() == typeof(double) && input.SurfaceTemp.GetType() == typeof(double))
                {
                    return true;
                }

            }
            return false;
        }
        public CylinderHeatingOutput Solve()
        {
            int error;
            Dictionary<string, List<double>> OutputDict = new Dictionary<string, List<double>>
            {
                { "Op", new List<double>() },
                { "Bio", new List<double>() },
                { "ConductTempCoef", new List<double>() },
                { "Pts", new List<double>() },
                { "Mts", new List<double>() },
                { "Nts", new List<double>() },
                { "Mu", new List<double>() },
                { "HeatTime", new List<double>() },
                { "Fo", new List<double>() },
                { "Oc", new List<double>() },
                { "Om", new List<double>() },
                { "Tc", new List<double>() },
                { "Tm", new List<double>() }
            };
            double[] SurfaceTemp_arr = new double[100];
            double tc_Check; // проверка tc <= 1, иначе будет отрицательная температура оси
            FillTempArray(SurfaceTemp_arr, SurfaceTemp, InitCylTemp);
            for (int i = 0; i < SurfaceTemp_arr.Length; i++)
            {
                OutputDict["Op"].Add(Math.Round((FurnaceTemp - SurfaceTemp_arr[i]) / (FurnaceTemp - InitCylTemp), 3));
                OutputDict["Bio"].Add(Math.Round(HeatTransferCoeff * Diameter / HeatConductCoeff / 2, 4));
                OutputDict["ConductTempCoef"].Add(HeatConductCoeff / HeatCapacity / Density);
                try
                {
                    int j = 0;
                    while (j < 23)
                    {
                        if (list.Rows[j].Bio > OutputDict["Bio"][i])
                        {
                            OutputDict["Pts"].Add(FindBioCoeff(OutputDict["Bio"][i], list.Rows[j - 1].Bio, list.Rows[j].Bio, list.Rows[j - 1].Pts, list.Rows[j].Pts));
                            OutputDict["Mts"].Add(FindBioCoeff(OutputDict["Bio"][i], list.Rows[j - 1].Bio, list.Rows[j].Bio, list.Rows[j - 1].Mts, list.Rows[j].Mts));
                            OutputDict["Nts"].Add(FindBioCoeff(OutputDict["Bio"][i], list.Rows[j - 1].Bio, list.Rows[j].Bio, list.Rows[j - 1].Nts, list.Rows[j].Nts));
                            OutputDict["Mu"].Add(FindBioCoeff(OutputDict["Bio"][i], list.Rows[j - 1].Bio, list.Rows[j].Bio, list.Rows[j - 1].Mu, list.Rows[j].Mu));
                            break;
                        }
                        j++;
                    }
                }
                catch (ArgumentOutOfRangeException e)
                {
                    break;
                }
                if (OutputDict["Pts"][i] != 0)
                {
                    OutputDict["HeatTime"].Add(Math.Round(HeatTime(Diameter, OutputDict["ConductTempCoef"][i], OutputDict["Pts"][i], OutputDict["Mu"][i], OutputDict["Op"][i])));

                }
                OutputDict["Fo"].Add(Math.Round(Forie(OutputDict["ConductTempCoef"][i], OutputDict["HeatTime"][i], Diameter), 2));
                OutputDict["Oc"].Add(Math.Round(TempDifference(OutputDict["Nts"][i], OutputDict["Mu"][i], OutputDict["Fo"][i]), 3));
                OutputDict["Om"].Add(TempDifference(OutputDict["Mts"][i], OutputDict["Mu"][i], OutputDict["Fo"][i]));
                tc_Check = Math.Round(FurnaceTemp - OutputDict["Oc"][i] * (FurnaceTemp - InitCylTemp));
                if (tc_Check < 0)
                {
                    break;
                }
                OutputDict["Tc"].Add(tc_Check);
                OutputDict["Tm"].Add(Math.Round(FurnaceTemp - OutputDict["Om"][i] * (FurnaceTemp - InitCylTemp)));
            }

            var model = new CylinderHeatingOutput()
            {
                Rows = new List<CylinderHeatingOutputRow>()
            };
            for (int i = OutputDict["Tc"].Count - 1; i >= 0; i--)
            {
                var row = new CylinderHeatingOutputRow()
                {
                    Op = OutputDict["Op"][i],
                    Bio = OutputDict["Bio"][i],
                    ConductTempCoef = OutputDict["ConductTempCoef"][i],
                    Pts = OutputDict["Pts"][i],
                    Mts = OutputDict["Mts"][i],
                    Nts = OutputDict["Nts"][i],
                    U = OutputDict["Mu"][i],
                    HeatTime = OutputDict["HeatTime"][i],
                    Fo = OutputDict["Fo"][i],
                    Oc = OutputDict["Oc"][i],
                    Om = OutputDict["Om"][i],
                    Tc = OutputDict["Tc"][i],
                    Tm = OutputDict["Tm"][i],
                    ActualSurfaceTemp = SurfaceTemp_arr[i]
                };
                model.Rows.Add(row);
            }
            return model;

        }
        public double FindBioCoeff(double calcBio, double tableBio, double tableBioNext, double tableCoeff, double tableCoeffNext)
        {
            double result = tableCoeff - (tableCoeff - tableCoeffNext) * (calcBio - tableBio) / (tableBioNext - tableBio);
            return result;
        }
        public double HeatTime(double diam, double ConductTempCoef, double Pts, double Mu, double Op)
        {
            double result = (double)(diam * diam / (4 * ConductTempCoef * Mu) * Math.Log(Pts / Op));
            return result;
        }
        public double Forie(double ConductTempCoef, double time, double diam)
        {
            double result = 4 * ConductTempCoef * time / (diam * diam);
            return result;
        }
        public double TempDifference(double BioParam, double Mu, double Fo)
        {
            double result = (double)(BioParam * Math.Exp(-Mu * Fo));
            return result;
        }
        public double[] FillTempArray(double[] array, double SurfaceTemp, double InitCylTemp)
        {
            array[0] = SurfaceTemp;
            double tempdiff = (SurfaceTemp - InitCylTemp) / 100;
            for (int i = 1; i < array.Length; i++)
            {
                array[i] = SurfaceTemp - tempdiff * i;
            }
            return array;
        }
    }
    
}