﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CylinderHeatingLibrary
{
    public class CylinderHeatingBios
    {
        public List<CylinderHeatingBiosRow>? Rows { get; set; }

    }
    public class CylinderHeatingBiosRow
    {
        public double Bio { get; set; }
        public double Mu { get; set; }
        public double Pts { get; set; }
        public double Mts { get; set; }
        public double Nts { get; set; }
       

    }
    
}
