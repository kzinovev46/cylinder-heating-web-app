﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CylinderHeatingLibrary
{
    public class CylinderHeatingInput
    {
        public string Name { get; set; }
        public double FurnaceTemp { get; set; } // Температура печи
        public double Diameter { get; set; } // Диаметр цилиндра
        public double InitCylTemp { get; set; } // Начальная температура цилиндра
        public double HeatConductCoeff { get; set; } // Коэффициент теплопроводности
        public double HeatCapacity { get; set; } // Теплоемкость
        public double Density { get; set; }  // Плотность
        public double HeatTransferCoeff { get; set; } // Коэффициент теплоотдачи
        public double SurfaceTemp { get; set; } // Температура поверхности цилиндра

        
    }

}
