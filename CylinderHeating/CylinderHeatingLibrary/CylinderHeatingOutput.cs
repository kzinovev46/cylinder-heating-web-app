﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CylinderHeatingLibrary
{
    public class CylinderHeatingOutput
    {
        public List<CylinderHeatingOutputRow> Rows { get; set; }
    }
    public class CylinderHeatingOutputRow
    {
        public double Op {get; set;}
        public double Bio {get; set;}
        public double ConductTempCoef {get; set;}
        public double Pts {get; set;}
        public double Mts {get; set;}
        public double Nts {get; set;}
        public double U {get; set;}
        public double HeatTime {get; set;}
        public double Fo {get; set;}
        public double Oc {get; set;}
        public double Om {get; set;}
        public double Tc { get; set;}
        public double Tm { get; set; }
        public double ActualSurfaceTemp { get; set;}
    }
    
}
