﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;


namespace RecuperatorLibrary
{
    public class HashPass
    {
        public static string hashPassword(string password)
        {
            string sault = Math.Round(Math.Log(password.Length), 5).ToString(); // соль - натуральный логарифм от длины пароля
            string passWithSault = password + sault;
            byte[] encodedPassword = new UTF8Encoding().GetBytes(passWithSault);

            // need MD5 to calculate the hash
            byte[] hash = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(encodedPassword);

            // string representation (similar to UNIX format)
            string encoded = BitConverter.ToString(hash);
            string enc = encoded.Replace("-", string.Empty);
            return enc;
        }
    }
}
